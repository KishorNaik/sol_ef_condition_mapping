﻿using Sol_EF_ConditionMapping.EF;
using Sol_EF_ConditionMapping.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_ConditionMapping
{
    class EmployeeDal
    {
        #region Declaration
        private EmployeeDBEntities db = null;
        #endregion

        #region Constructor 
        public EmployeeDal()
        {
            db = new EmployeeDBEntities();
        }
        #endregion

        #region Public Method
        public async  Task<IEnumerable<EmployeeEntity>> GetEmployeeTerminateDate()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                        db
                        ?.tblEmployees
                        ?.AsEnumerable()
                        ?.Select((letblEmployeeObj) => new EmployeeEntity()
                        {
                            EmployeeId = (int?)letblEmployeeObj.EmployeeId,
                            FirstName = letblEmployeeObj.FirstName,
                            LastName = letblEmployeeObj.LastName,
                            IsTerminate = true
                        })
                        ?.ToList();

                    return getQuery;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
